#!/bin/sh -e
#
# OHAI-Kit Docker startup script
#
# Copyright (C) 2021 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

MANAGE_CMD="./manage.py"
WSGI_SERVER="gunicorn"
DJANGO_APP="ohai"

prepare() {
	"$MANAGE_CMD" collectstatic --noinput --clear
	"$MANAGE_CMD" compilescss --use-processor-root
	"$MANAGE_CMD" migrate --noinput
}

run() {
	prepare
	exec "$WSGI_SERVER" "$DJANGO_APP".wsgi
}

if [ "$1" = "ohai" ]; then
	run
fi

exec "$@"
